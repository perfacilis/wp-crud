<?php

namespace Perfacilis;

/**
 * Abstract CRUD (Create, Read, Update, Delete) class to support the lifecycle
 * of the most (simple) entities.
 * - Make sure the constructor of the final class enforces required arguments
 *   (datatypes will be checked here).
 *
 * @author roy <support@perfacilis.com>
 */
abstract class WpCrud
{
    public static function get(array $filter)
    {
        global $wpdb;

        $sql = [];
        $params = [];

        $ordering = self::getOrder($filter);
        $order = $ordering['order'];
        $dir = $ordering['dir'];
        $params['order'] = $ordering['sql'];

        $per_page = static::getPerPage();
        $params['limit'] = isset($filter['limit']) ? (int) $filter['limit'] : $per_page;
        $params['offset'] = isset($filter['offset']) ? (int) $filter['offset'] : 0;

        // Return array
        $get = [
            'data' => [],
            'order' => $order,
            'direction' => $dir,
            'count' => 0,
            'per_page' => $per_page,
        ];

        // Build the sql filter based on available columns
        self::getSqlFilterTraits($filter, $sql, $params);
        static::getSqlFilter($filter, $sql, $params);

        // Try to avoid SQL_CALC_FOUND_ROWS
        // !!! This needs to be tested with compound primary keys
        $pkey = static::getPrimaryKey();
        foreach ($pkey as $colname) {
            // Match if $filter["id"] has been set
            if (isset($params[$colname])) {
                $params['limit'] = 0;
            }
            // Match is $filter["ids"] has been set
            elseif (isset($params[$colname . 's'])) {
                $params['limit'] = 0;
            }
        }

        $columns = static::getColumns();

        $query = 'SELECT ';
        $query .= $params['limit'] >= $per_page ? 'SQL_CALC_FOUND_ROWS ' : '';
        $query .= '`' . implode('`, `', array_keys($columns)) . '` ';
        $query .= 'FROM `' . static::getTable() . '` ';
        $query .= $sql ? 'WHERE ' . implode(' AND ', $sql) . ' ' : '';
        $query .= 'ORDER BY ' . $params['order'] . ' ';
        $query .= $params['limit'] ? 'LIMIT :offset, :limit' : '';

        // Prepare query
        $query = static::prepareQuery($query, $params);

        $i = 0;
        while ($row = $wpdb->get_row($query, ARRAY_A, $i)) {
            foreach ($columns as $col_name => $data_type) {
                $value = self::setDataType($row[$col_name], $data_type);
                $row[$col_name] = $value;
            }

            $id = self::getObjectId($row);
            $get['data'][$id] = $row;

            $i += 1;
        }

        $get['count'] = $wpdb->num_rows;
        if ($params['limit'] >= $per_page) {
            $count = $wpdb->get_var('SELECT FOUND_ROWS();');
            $get['count'] = (int) $count;
        }

        $wpdb->flush();

        return $get;
    }

    public static function all(array $filter = [])
    {
        $filter['limit'] = 0;
        return self::get($filter);
    }

    /**
     * Get first item sorting by primary key
     * @param array $filter
     * @return array
     */
    public static function first(array $filter = []): array
    {
        $filter['limit'] = 1;
        $get = self::get($filter);

        if (!$get['data']) {
            return [];
        }

        return reset($get['data']);
    }

    /**
     * Get last item, sorting by primary key
     * @param array $filter
     * @return array
     */
    public static function last(array $filter = []): array
    {
        $filter['limit'] = 1;
        $filter['dir'] = 'desc';

        $get = self::get($filter);
        if (!$get['count']) {
            return [];
        }

        return reset($get['data']);
    }

    /**
     * Create entity instance, responsible for Create and Read steps
     *
     * @return void
     * @throws \Exception
     */
    public function __construct(...$arguments)
    {
        $this->bootTraits();

        $pkey = $this->getParsedPrimaryKey();

        // Create: not all arguments for pkey are set
        $is_new = false;
        foreach (array_keys($pkey) as $i => $field) {
            if (empty($arguments[$i])) {
                $is_new = true;
                break;
            }
        }

        $this->is_new = $is_new;
        if ($this->is_new) {
            return $this->setDefaults();
        }

        // Read: all arguments for pkey must be set
        foreach (array_keys($pkey) as $i => $field) {
            $datatype = $pkey[$field];
            $this->{$field} = $this->setDataType($arguments[$i], $datatype);
        }

        $filter = [];
        foreach (array_keys($pkey) as $field) {
            $filter[$field] = $this->{$field};
        }

        // Try to retrieve current entitiy
        $get = self::get($filter);
        $objectid = $this->getObjectId($filter);

        if (!isset($get['data'][$objectid])) {
            $this->setDefaults();
            return $this->is_new = true;
        }

        $object = &$get['data'][$objectid];
        foreach ($object as $field => $value) {
            $this->{$field} = $value;
        }
    }

    public function isNew()
    {
        return $this->is_new;
    }

    public function setNew($is_new)
    {
        $this->is_new = $is_new;
    }

    /**
     * Save entity, responsible for Create and Update step. If no compound key
     * is used, the internal primary key will be set.
     *
     * @return bool TRUE if Update or Insert was successful.
     * @global \wpdb $wpdb
     */
    public function save()
    {
        $this->saveTraits();

        global $wpdb;

        $column_types = self::getColumns();
        $pkey = self::getParsedPrimaryKey();

        $data = [];
        $format = [];

        $where = [];
        $where_format = [];

        foreach (array_keys($column_types) as $column) {
            // Use primary key as condition
            if (isset($pkey[$column])) {
                $where[$column] = $this->{$column};
                $where_format[$column] = self::getDbType($where[$column]);
                continue;
            }

            // Otherwise set it to be updated
            $data[$column] = $this->{$column};
            $format[$column] = self::getDbType($data[$column]);
        }

        // Update existing entitiy
        if (!$this->is_new) {
            // Entity consists of key only, nothing to do
            if (!$data) {
                return true;
            }

            return $wpdb->update($this->getTable(), $data, $where, $format, $where_format) !== false;
        }

        $data = array_merge($data, $where);
        $format = array_merge($format, $where_format);

        // Create new entity
        if (!$wpdb->insert($this->getTable(), $data, $format)) {
            return false;
        }

        // Unset new, so when next time save is called it'll perform an Update
        $this->is_new = false;

        // Insert id will not be available for compound keys, so just return
        if (count($pkey) > 1) {
            return true;
        }

        $id_field = key($pkey);
        $insert_id = (int) $wpdb->insert_id;

        // Set primary key with new AUTO_INCREMENT value, if applicable
        if ($insert_id) {
            $this->{$id_field} = $insert_id;
        }

        return !empty($this->{$id_field});
    }

    /**
     * Delete entity, responsible for Delete step.
     * Sets is_new back to TRUE on success.
     * WARNING: Removes entry from db unless SoftDelete trait is used.
     *
     * @return bool TRUE on success
     */
    public function delete(): bool
    {
        if ($this->is_new) {
            return true;
        }

        global $wpdb;

        $where = [];
        $where_format = [];

        $pkey = static::getPrimaryKey();
        foreach ($pkey as $field) {
            $value = $this->{$field};
            $where[$field] = $value;
            $where_format = self::getDbType($value);
        }

        if (!$wpdb->delete($this->getTable(), $where, $where_format)) {
            return false;
        }

        return $this->is_new = true;
    }

    protected function setDefaults()
    {
    }

    /**
     * @return array E.g. ['id'] or ['order','product']
     */
    protected static function getPrimaryKey(): array
    {
        return ['id'];
    }

    /**
     * @return array ['order' => '`field` %1$s']
     */
    protected static function getOrders(): array
    {
        $orders = [];

        $pkey = static::getPrimaryKey();
        if (count($pkey) > 1) {
            $orders['pkey'] = '`' . implode('` %1$s, `', static::getPrimaryKey()) . '` %1$s';
        }

        foreach (static::getPrimaryKey() as $field) {
            $orders[$field] = '`' . $field . '` %1$s';
        }

        return $orders;
    }

    /**
     * @return int
     */
    protected static function getPerPage(): int
    {
        return 10;
    }

    /**
     * @return string
     * @global \wpdb $wpdb
     */
    protected static function getTable(): string
    {
        global $wpdb;

        $class = str_replace('\\', '', static::class);

        // Convert camelCase to lower_case string
        $class = preg_replace_callback('~[A-Z]+~', function ($match) {
            static $nth = 0;
            return ($nth++ == 0 ? '' : '_') . strtolower($match[0]);
        }, $class);

        return $wpdb->prefix . $class;
    }

    protected static function getOrder(array $filter)
    {
        $child = get_called_class();
        $orders = $child::getOrders();

        $order = isset($filter['order'], $orders[$filter['order']]) ? $filter['order'] : key($orders);
        $dirs = isset($orders[$order]) ? $orders[$order] : [];

        if (isset($filter['direction'])) {
            $filter['dir'] = $filter['direction'];
        }

        // Single, simple, entry: "`colname` %1$s"
        $dir = !empty($filter['dir']) ? $filter['dir'] : 'ASC';
        $sql = sprintf($dirs, $dir);

        return [
            'order' => $order,
            'dir' => $dir,
            'sql' => $sql,
        ];
    }

    /**
     * Retrieves entity columns with their proper data types
     *
     * @return array [colname => dtype]
     */
    protected static function getColumns()
    {
        static $parsed_columns = [];

        if (isset($parsed_columns[static::class])) {
            return $parsed_columns[static::class];
        }

        $ref = new \ReflectionClass(static::class);
        $defaults = $ref->getDefaultProperties();

        foreach ($ref->getProperties() as $property) {
            if ($property->isPrivate() || $property->isStatic()) {
                continue;
            }

            $colname = $property->getName();
            $value = $defaults[$colname];
            $parsed_columns[static::class][$colname] = gettype($value);
        }

        return $parsed_columns[static::class];
    }

    protected static function getParsedPrimaryKey()
    {
        static $parsed_primary_key = [];

        if (isset($parsed_primary_key[static::class])) {
            return $parsed_primary_key[static::class];
        }

        $primary_key = static::getPrimaryKey();

        $columns = static::getColumns();
        foreach ($primary_key as $field) {
            $type = $columns[$field];
            $parsed_primary_key[static::class][$field] = $type;
        }

        return $parsed_primary_key[static::class];
    }

    protected static function getSqlFilter($filter, &$sql, &$params): void
    {
        $columns = static::getColumns();
        foreach ($columns as $colname => $type) {
            if (!isset($filter[$colname])) {
                continue;
            }

            $var = $filter[$colname];
            if (is_array($var)) {
                $var = self::setDataType($var, 'array_' . $type);
                $sql[] = '`' . $colname . '` IN('
                    . (strpos($type, 'string') !== false ?
                    '\'' . implode('\', \'', array_map('esc_sql', $var)) . '\'' :
                    implode(',', $var))
                    . ')';
            } else {
                $var = self::setDataType($var, $type);
                $sql[] = '`' . $colname . '` = ' . (is_string($var) ? '\'' . esc_sql($var) . '\'' : $var);
                $params[$colname] = $var;
            }
        }
    }

    protected static function getDbType($value)
    {
        $type = gettype($value);
        switch ($type) {
            case 'string':
                return '%s';
            // break;

            case 'float':
            case 'double':
                return '%f';

            case 'bool':
            case 'boolean':
            case 'int':
            case 'integer':
                return '%d';
            // break;
        }

        throw new \Exception('Datatype \'' . $type . '\' is not implemented: ' . var_export($value, true));
    }

    /**
     * Sets datatype of given var, like settype(), though this function accepts
     * non-standard types used by the \MySql class.
     *
     * @param mixed $var
     * @param string $type
     * @return mixed
     */
    private static function setDataType($var, $type)
    {
        switch ($type) {
            case 'bool':
            case 'boolean':
            case 'double':
            case 'float':
            case 'integer':
            case 'string':
                // works as is
                break;

            case 'array_int':
            case 'array_integer':
                $var = array_filter($var, 'is_int');
                $type = 'array';
                break;

            case 'array_string':
                $type = 'array';
                break;

            default:
                throw new \Exception('Given datatype \'' . $type . '\' not implemented.');
            // break;
        }

        settype($var, $type);
        return $var;
    }

    /**
     * Generates a unique object id (certainly not a UUID) by which objects are
     * identified. This is done by concatenating the field values of the primary
     * key.
     * For simple primary keys, it returns "123"
     * For compound primary keys, it returns "123.456" or "1.foo.bar"
     *
     * @param array $object_data
     * @return string
     */
    private static function getObjectId(array $object_data)
    {
        $class = get_called_class();
        $primary_key = $class::getPrimaryKey();

        $objectid = [];
        foreach ($primary_key as $field) {
            $objectid[] = $object_data[$field];
        }

        return implode('.', $objectid);
    }

    private static function getSqlFilterTraits($filter, &$sql, &$params)
    {
        $traits = class_uses(get_called_class());
        foreach ($traits as $trait) {
            $save_method = 'getSqlFilter' . substr($trait, strrpos($trait, '\\') + 1);
            if (method_exists($trait, $save_method)) {
                static::{$save_method}($filter, $sql, $params);
            }
        }
    }

    private static function prepareQuery(string $query, array $params): string
    {
        global $wpdb;

        $matches = [];
        if (!preg_match_all('~:([a-z]+)~', $query, $matches)) {
            return $query;
        }

        $args = [];
        foreach ($matches[0] as $i => $search) {
            $name = $matches[1][$i];
            if (!isset($params[$name])) {
                throw new \Exception(sprintf('Cannot replace \'%s\' in query, because param \'%s\' doesn\'t exist.', $search, $name));
            }

            $args[] = $params[$name];

            $replace = self::getDbType($params[$name]);
            $query = str_replace($search, $replace, $query);
        }

        return $wpdb->prepare($query, $args);
    }

    private $is_new = true;

    /**
     * Call boot methods from traits
     */
    private function bootTraits()
    {
        $traits = class_uses(get_called_class());
        foreach ($traits as $trait) {
            $boot_method = 'boot' . substr($trait, strrpos($trait, '\\') + 1);
            if (method_exists($trait, $boot_method)) {
                $this->{$boot_method}();
            }
        }
    }

    private function saveTraits()
    {
        $traits = class_uses(get_called_class());
        foreach ($traits as $trait) {
            $save_method = 'save' . substr($trait, strrpos($trait, '\\') + 1);
            if (method_exists($trait, $save_method)) {
                $this->{$save_method}();
            }
        }
    }
}
