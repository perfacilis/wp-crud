<?php

namespace Perfacilis\WpCrud;

/**
 * Description of HasOwner
 *
 * @author roy <support@perfacilis.com>
 */
trait HasOwner
{
    public function getCreatedBy(): int
    {
        return $this->created_by;
    }

    public function setCreatedBy(int $user_id): void
    {
        $this->created_by = $user_id;
    }

    public function getModifiedBy(): int
    {
        return $this->modified_by;
    }

    public function setModifiedBy(int $user_id): void
    {
        $this->modified_by = $user_id;
    }

    protected $created_by = 0;
    protected $modified_by = 0;

    protected function bootHasOwner()
    {
        $user_id = get_current_user_id();
        $this->created_by = $user_id;
        $this->modified_by = $user_id;
    }

    protected function saveHasOwner()
    {
        $user_id = get_current_user_id();
        if (!$this->created_by) {
            $this->created_by = $user_id;
        }

        $this->modified_by = $user_id;
    }
}
