<?php

namespace Perfacilis\WpCrud;

/**
 * Description of Times
 *
 * @author roy <support@perfacilis.com>
 */
trait HasTimes
{
    public function getCreatedAt(): int
    {
        return $this->created_at;
    }

    public function setCreatedAt(int $unix_timestamp): void
    {
        $this->created_at = $unix_timestamp;
    }

    public function getModifiedAt(): int
    {
        return $this->modified_at;
    }

    public function setModifiedAt(int $unix_timestamp): void
    {
        $this->modified_at = $unix_timestamp;
    }

    protected $created_at = 0;
    protected $modified_at = 0;

    protected function bootHasTimes()
    {
        $now = time();
        $this->created_at = $now;
        $this->modified_at = $now;
    }

    protected function saveHasTimes()
    {
        $now = time();

        if (!$this->created_at) {
            $this->created_at = $now;
        }

        $this->modified_at = $now;
    }
}
