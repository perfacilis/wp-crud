<?php

namespace Perfacilis\WpCrud;

/**
 * Description of SoftDelete
 *
 * @author roy <support@perfacilis.com>
 */
trait SoftDelete
{

    /**
     * @return int Unix timestamp
     */
    public function getDeletedAt(): int
    {
        return $this->deleted_at;
    }

    /**
     * @return int User id
     */
    public function getDeletedBy(): int
    {
        return $this->deleted_by;
    }

    /**
     * Remove item from database.
     * Use softDelete methods to mark item deleted instead.
     *
     * @return bool
     * @see WpCrud::delete()
     */
    public function hardDelete(): bool
    {
        return parent::delete();
    }

    /**
     * Mark item as deleted by setting deleted_by and deleted_time, but doesn't
     * actually remove item from database.
     *
     * @return bool
     */
    public function softDelete(): bool
    {
        $this->deleted_by = get_current_user_id();
        $this->deleted_at = time();

        return $this->save();
    }

    /**
     * Alias for softDelete, mark item as deleted.
     * @return bool
     */
    public function delete(): bool
    {
        return $this->softDelete();
    }

    /**
     * Alias for softDelete, mark item as deleted.
     * @return bool
     */
    public function trash(): bool
    {
        return $this->softDelete();
    }

    /**
     * @return bool TRUE if item is marked as deleted.
     */
    public function isDeleted(): bool
    {
        return $this->deleted_at !== 0 || $this->deleted_by !== 0;
    }

    /**
     * Remove item from trash and mark it as 'not deleted'.
     * @return void
     */
    public function restore(): void
    {
        $this->deleted_at = 0;
        $this->deleted_by = 0;
        $this->save();
    }

    protected $deleted_at = 0;
    protected $deleted_by = 0;

    protected static function getSqlFilterSoftDelete($filter, &$sql, &$params): void
    {
        $show_deleted = isset($filter['show_deleted']) ? (bool) $filter['show_deleted'] : false;
        if (!$show_deleted) {
            $sql[] = '`deleted_at` = 0';
            $sql[] = '`deleted_by` = 0';
            $params['deleted_at'] = 0;
            $params['deleted_by'] = 0;
        }
    }
}
