# README #

Composer library for creating CRUD objects in Wordpress, using Wordpress Database layer.

### Installation

Add this to your compser.json:
```
    "repositories": [
        {
            "type": "vcs",
            "url": "git@bitbucket.org:perfacilis/wp-crud.git"
        },
    ]
```

After saving, add the requirement:
```
composer require perfacilis/wp-crud:dev-master
```

After that it can be used in php:

```
use Perfacilis/WpCrud as Crud;

class Item extends Crud { ... }

```
